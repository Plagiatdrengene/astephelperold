using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using aStepHelper.Data;
using aStepHelper.Types;
using Microsoft.AspNetCore.Mvc;

namespace aStepHelper {
	public class FieldFactory {
		public List<Field> Fields { get; } = new List<Field>();

		public void CreateSelect(string label, Option[] options) {
			if (options == null || options.Length == 0)
				throw new Exception("You need to have options!");
			Fields.Add(new Field(label, options[0].Value, FieldType.Select, options));
		}

		public void CreateCheckBox(string label, bool defaultValue) {
			Fields.Add(new Field(label, defaultValue.ToString().ToLower(), FieldType.Checkbox));
		}

		public void CreateInput(string label, string placeholder = "") {
			Fields.Add(new Field(label, placeholder, FieldType.Input));
		}

		public void CreateNumberInput(string label, string placeholder = "") {
			if (!Regex.IsMatch(placeholder, "^[0-9]+$"))
				throw new Exception("Contains other than numbers in placeholder!");
			Fields.Add(new Field(label, placeholder, FieldType.InputNumber));
		}

		/// <summary>
		/// Gets the fields as a content result.
		/// </summary>
		/// <returns>The fields as a content result.</returns>
		public ContentResult GetResult() {
			return Fields.ToContentResult();
		}
	}
}