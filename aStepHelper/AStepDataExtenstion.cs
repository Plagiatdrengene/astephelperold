using System.Collections.Generic;
using aStepHelper.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace aStepHelper {
	public static class AStepDataExtenstion {
		public static ContentResult ToContentResult(this IAStepData value) {
			return new ContentResult {Content = JsonConvert.SerializeObject(value), ContentType = "application/json"};
		}

		public static ContentResult ToContentResult(this IEnumerable<IAStepData> value) {
			return new ContentResult {Content = JsonConvert.SerializeObject(value), ContentType = "application/json"};
		}

		public static string GetValueFromField(this IFormCollection collection, string fieldName) {
			if (collection.ContainsKey(fieldName))
				return collection[fieldName][0];
			throw new KeyNotFoundException($"The key {fieldName} was not found.");
		}

		public static string GetValueFromField(this IFormCollection collection, Field field) {
			return collection.GetValueFromField(field.Name);
		}
	}
}