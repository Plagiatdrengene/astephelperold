using aStepHelper.Data.ChartData;
using Newtonsoft.Json;

namespace aStepHelper.ChartData {
	public class GeographicalCluster : IAStepChart {
		[JsonProperty("view")] public GeoView GeoView;
		[JsonProperty("points")] public CoordinateSet[] Points;

		public GeographicalCluster(GeoView view, CoordinateSet[] points) {
			GeoView = view;
			Points = points;
		}
	}
}