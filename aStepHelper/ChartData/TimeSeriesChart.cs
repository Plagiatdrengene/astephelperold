using System.Collections.Generic;
using Newtonsoft.Json;

namespace aStepHelper.ChartData {
	public class TimeSeriesChart : IAStepChart {
		[JsonProperty("chartData")] public Dictionary<string, TimeSeriesData> TimeSeriesData;

		public TimeSeriesChart(Dictionary<string, TimeSeriesData> timeSeriesData) {
			TimeSeriesData = timeSeriesData;
		}
	}

	public class TimeSeriesData {
		[JsonProperty("data")] public List<float> Data;
		[JsonProperty("inputSize")] public int Size;
		[JsonProperty("predictions")] public List<Prediction> Predictions;

		public TimeSeriesData(List<float> data, List<Prediction> predictions, int size = 3) {
			Data = data;
			Size = size;
			Predictions = predictions;
		}
	}

	public class Prediction {
		[JsonProperty("data")] public List<float> Data;
		[JsonProperty("error")] public Dictionary<string, float> Error;

		public Prediction(List<float> data, Dictionary<string, float> error) {
			Data = data;
			Error = error;
		}
	}
}