using System.Collections.Generic;
using aStepHelper.Data;
using Newtonsoft.Json;

namespace aStepHelper.ChartData {
	[JsonConverter(typeof(AStepEnumConverter))]
	public class Composite : IAStepChart {
		public readonly List<Content> Contents = new List<Content>();

		public void AddChart(string name, Content content) {
			content.Name = name;
			Contents.Add(content);
		}
	}
}