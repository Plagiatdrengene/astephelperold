using aStepHelper.Data.ChartData;
using aStepHelper.Types;
using Newtonsoft.Json;

namespace aStepHelper.ChartData {
	public class JsChart : IAStepChart {
		[JsonProperty("data")] public JsDataSet[] DataSets;
		[JsonProperty("labels")] public string[] Labels;

		[JsonProperty("colors")] [JsonConverter(typeof(AStepEnumConverter))]
		public JsChartColor Colors;

		[JsonConverter(typeof(AStepEnumConverter))] [JsonProperty("type")]
		public JsChartType Type;

		public JsChart(JsChartType type, JsDataSet[] dataSets, string[] labels) {
			Type = type;
			DataSets = dataSets;
			Labels = labels;
			Colors = new JsChartColor();
		}
	}
}