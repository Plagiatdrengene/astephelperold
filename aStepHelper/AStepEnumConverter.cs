using System;
using System.Drawing;
using aStepHelper.ChartData;
using aStepHelper.Data;
using aStepHelper.Data.ChartData;
using Newtonsoft.Json;

namespace aStepHelper {
	public class AStepEnumConverter : JsonConverter {
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
			switch (value) {
				case Composite composite:
					TransformToComposite(writer, composite, serializer);
					return;
				case JsChartColor jsChartColor:
					TransformToColor(writer, jsChartColor);
					return;
				default:
					switch (value.ToString()) {
						case "InputNumber":
							writer.WriteValue("input-number");
							break;
						case "MapCluster":
							writer.WriteValue("map-cluster");
							break;
						case "ChartJs":
							writer.WriteValue("chart-js");
							break;
						case "GenericTimeSeries":
							writer.WriteValue("generic-time-series");
							break;
						default:
							writer.WriteValue(value.ToString().ToLower());
							break;
					}

					break;
			}
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
			JsonSerializer serializer) {
			var enumString = (string) reader.Value;
			return Enum.Parse(typeof(Enum), enumString, true);
		}

		public override bool CanConvert(Type objectType) {
			return objectType == typeof(string);
		}

		private static void TransformToColor(JsonWriter writer, JsChartColor jsChartColor) {
			writer.WriteStartArray();
			foreach ((string key, Color color) in jsChartColor.Colors) {
				writer.WriteStartObject();
				writer.WritePropertyName(key);
				writer.WriteValue("rgba(" + color.R + ", " + color.G + ", " + color.B + "," +
				                  color.A / 255 + ")");
				writer.WriteEndObject();
			}

			writer.WriteEndArray();
		}

		private static void TransformToComposite(JsonWriter writer, Composite composite, JsonSerializer serializer) {
			writer.WriteStartArray();
			foreach (Content content in composite.Contents) {
				serializer.Serialize(writer, content);
			}

			writer.WriteEndArray();
		}
	}
}