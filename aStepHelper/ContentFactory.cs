using System.Collections.Generic;
using System.IO;
using aStepHelper.ChartData;
using aStepHelper.Data;
using aStepHelper.Data.ChartData;
using aStepHelper.Types;

namespace aStepHelper {
	public static class ContentFactory {
		public static Content CreateMarkdown(string markdown) {
			return new Content(ChartType.Markdown, markdown);
		}

		public static Content CreateMarkdownFromFile(string path) {
			if (!File.Exists(path)) throw new FileNotFoundException();
			return new Content(ChartType.Markdown, File.ReadAllText(path));
		}

		public static Content CreateText(string text) {
			return new Content(ChartType.Text, text);
		}

		public static Content CreateGeoClusterChart(GeoView view, CoordinateSet[] points) {
			return new Content(ChartType.MapCluster, new GeographicalCluster(view, points));
		}

		public static Content CreateJsChart(JsChartType chartType, JsDataSet[] dataSets, string[] labels) {
			var data = new JsChart(chartType, dataSets, labels);
			return new Content(ChartType.ChartJs, data);
		}

		public static Content CreateComposite(Composite composite) {
			return new Content(ChartType.Composite, composite);
		}

		public static Content CreateExampleTimeSeriesChart() {
			List<Prediction> prediction = new List<Prediction>();
			prediction.Add(new Prediction(new List<float>() {
				300,
				300,
				300,
				300,
				300
			}, new Dictionary<string, float>() {
				{"mae", 60.0f},
				{"smape", 17.320508075688775f}
			}));

			prediction.Add(new Prediction(new List<float>() {
				400,
				400,
				400,
				400,
				400
			}, new Dictionary<string, float>() {
				{"mae", 80.0f},
				{"smape", 20f}
			}));

			prediction.Add(new Prediction(new List<float>() {
				500,
				500,
				500,
				500,
				500
			}, new Dictionary<string, float>() {
				{"mae", 100.0f},
				{"smape", 22.360679774997898f}
			}));

			prediction.Add(new Prediction(new List<float>() {
				600,
				600,
				600,
				600,
				600
			}, new Dictionary<string, float>() {
				{"mae", 120.0f},
				{"smape", 24.49489742783178f}
			}));

			prediction.Add(new Prediction(new List<float>() {
				700,
				700,
				700,
				700,
				700
			}, new Dictionary<string, float>() {
				{"mae", 140.0f},
				{"smape", 26.457513110645905f}
			}));

			prediction.Add(new Prediction(new List<float>() {
				800,
				800,
				800,
				800,
				800
			}, new Dictionary<string, float>() {
				{"mae", 160.0f},
				{"smape", 28.284271247461902f}
			}));

			prediction.Add(new Prediction(new List<float>() {
				900,
				900,
				900,
				900,
				900
			}, new Dictionary<string, float>() {
				{"mae", 180.0f},
				{"smape", 30.0f}
			}));

			prediction.Add(new Prediction(new List<float>() {
				1000,
				1000,
				1000,
				1000,
				1000
			}, new Dictionary<string, float>() {
				{"mae", 200.0f},
				{"smape", 31.622776601683793f}
			}));

			var dab = new List<float>() {
				0,
				100,
				200,
				300,
				400,
				500,
				600,
				700,
				800,
				900,
				1000,
				1100,
				1200,
				1300,
				1400,
				1500
			};
			var data = new TimeSeriesData(dab, prediction);

			var chart = new TimeSeriesChart(new Dictionary<string, TimeSeriesData>() {{"Fun", data}});

			return new Content(ChartType.GenericTimeSeries, chart);
		}

		public static Content CreateTimeSeriesChart(TimeSeriesChart chart) {
			return new Content(ChartType.GenericTimeSeries, chart);
		}
	}
}