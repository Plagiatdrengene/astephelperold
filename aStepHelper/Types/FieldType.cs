namespace aStepHelper.Types {
	/// <summary>
	/// Enumeration for FieldType.
	/// </summary>
	public enum FieldType {
		Select,
		Checkbox,
		Input,
		InputNumber,

		//TODO: NOT SUPPORTED!!
		/*MultiSelect,
		CheckboxGroup,
		Button,
		FormsetSelect*/
	}
}