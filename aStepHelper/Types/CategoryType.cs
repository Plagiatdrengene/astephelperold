namespace aStepHelper.Types {
	/// <summary>
	/// Enumeration for CategoryType.
	/// </summary>
	public enum CategoryType {
		Other,
		PathAnalytics,
		TrajectoryAnalytics,
		TimeSeriesAnalytics
	}
}