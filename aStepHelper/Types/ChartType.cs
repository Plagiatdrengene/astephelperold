namespace aStepHelper.Types {
	/// <summary>
	/// Enumeration for ChartType.
	/// </summary>
	public enum ChartType {
		Text,
		Markdown,
		MapCluster,
		ChartJs,
		Composite,

		GenericTimeSeries
		//TODO: Not supported:
		/*	MapGeo,
			SocketIO,
			RoboticsClassification,
			Exoskeleton,
			Driver,
			PDF*/
	}
}