namespace aStepHelper.Types {
	public enum JsChartType {
		Line,
		Bar,
		Radar,
		Doughnut,
		Pie,

		//TODO: Below doesn't seem to work
		/*PolarArea,
		Bubble,
		Scatter*/
	}
}