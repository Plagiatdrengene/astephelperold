using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;

namespace aStepHelper.Data {
	/// <summary>
	/// Represents an Option that is to be displayed in a field.
	/// </summary>
	public sealed class Option : IAStepData {
		[NotNull] [JsonProperty("name")] public string Name;
		[NotNull] [JsonProperty("value")] public string Value;
		/// <summary>
		/// Constructs an Option given a name.
		/// </summary>
		/// <param name="name">The name of the option.</param>
		/// <exception cref="Exception">Exception thrown if the parameter name is all lowercase.</exception>
		public Option([NotNull] string name) {
			Name = name ?? throw new NoNullAllowedException();
			if (name.ToLower() == name)
				throw new Exception("It can't be all lowercase.");
			Value = name.ToLower();
		}
	}
}