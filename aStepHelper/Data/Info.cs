using System.Data;
using System.Diagnostics.CodeAnalysis;
using aStepHelper.Types;
using Newtonsoft.Json;

namespace aStepHelper.Data {
	/// <summary>
	/// Info for the specification of the name, version and category of a service/function in aSTEP.
	/// </summary>
	public sealed class Info : IAStepData {
		[NotNull] [JsonProperty("name")] public string Name;
		[NotNull] [JsonProperty("version")] public string Version;
		[JsonProperty("category")] public int Category;
		/// <summary>
		/// Constructs Info given a name, version and category.
		/// </summary>
		/// <param name="name">Name of the service/function.</param>
		/// <param name="version">The version of the service/function.</param>
		/// <param name="category">The category of the service/function.</param>
		public Info([NotNull] string name, [NotNull] string version, CategoryType category) {
			Name = name ?? throw new NoNullAllowedException();
			Version = version ?? throw new NoNullAllowedException();
			Category = ((int) category) - 1;
		}
	}
}