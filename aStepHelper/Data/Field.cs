using System.Data;
using System.Diagnostics.CodeAnalysis;
using aStepHelper.Types;
using Newtonsoft.Json;

namespace aStepHelper.Data {
	/// <summary>
	/// A representation of a Field that is to be displayed in a service/function in aSTEP.
	/// </summary>
	public sealed class Field : IAStepData {
		[NotNull] [JsonProperty("name")] public string Name;
		[NotNull] [JsonProperty("label")] public string Label;
		[NotNull] [JsonProperty("default")] public string DefaultValue;

		[NotNull] [JsonProperty("placeholder")]
		public string Placeholder;

		[JsonConverter(typeof(AStepEnumConverter))] [JsonProperty("type")]
		public FieldType Type;

		[JsonProperty("options")] public Option[] Options;

		/// <summary>
		/// Constructs Field given a label, placeholder and options for the field.
		/// </summary>
		/// <param name="label">The name of the field.</param>
		/// <param name="placeholder">Placeholder.</param>
		/// <param name="type">The type of the field.</param>
		/// <param name="options">The options in the field.</param>
		internal Field([NotNull] string label, [NotNull] string placeholder, FieldType type, Option[] options = null) {
			Label = label ?? throw new NoNullAllowedException();
			Name = (type + "." + label).ToLower();
			Placeholder = placeholder;
			if (type == FieldType.Select)
				DefaultValue = placeholder;
			Type = type;
			Options = options;
		}
	}
}