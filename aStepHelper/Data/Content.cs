using System.Data;
using System.Diagnostics.CodeAnalysis;
using aStepHelper.ChartData;
using aStepHelper.Types;
using Newtonsoft.Json;

namespace aStepHelper.Data {
	/// <summary>
	/// Content that is to be displayed when opening a service/function in aSTEP.
	/// </summary>
	public sealed class Content : IAStepData {
		[JsonProperty("name")] public string Name;

		[JsonConverter(typeof(AStepEnumConverter))]
		[JsonProperty("chart_type")]
		public ChartType ChartType { get; private set; }

		[JsonProperty("content")] [NotNull] public object ContentData { get; private set; }

		/// <summary>
		/// Constructs Content given a chart and data.
		/// </summary>
		/// <param name="chartType">The type of chart.</param>
		/// <param name="contentData">The ContentData</param>
		internal Content(ChartType chartType, [NotNull] IAStepChart contentData, string name = "") {
			ChartType = chartType;
			ContentData = contentData ?? throw new NoNullAllowedException();
			Name = name;
		}

		internal Content(ChartType chartType, [NotNull] string contentData, string name = "") {
			ChartType = chartType;
			ContentData = contentData ?? throw new NoNullAllowedException();
			Name = name;
		}
	}
}