namespace aStepHelper.Data {
	/// <summary>
	/// Interface for data utilized in aSTEP.
	/// </summary>
	public interface IAStepData { }
}