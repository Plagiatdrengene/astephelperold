using Newtonsoft.Json;

namespace aStepHelper.Data.ChartData {
	public class GeoView {
		[JsonProperty("center")] public CoordinateSet Center;
		[JsonProperty("zoom")] public float Zoom;

		public GeoView(double lat, double lon, float zoom = 10) {
			Center = new CoordinateSet(lat, lon);
			Zoom = zoom;
		}
	}
}