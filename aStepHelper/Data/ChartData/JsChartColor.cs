using System.Collections.Generic;
using System.Drawing;

namespace aStepHelper.Data.ChartData {
	public class JsChartColor {
		public readonly Dictionary<string, Color> Colors = new Dictionary<string, Color> {
			{
				"backgroundColor", Color.FromArgb(255, 148, 159, 177)
			}, {
				"borderColor", Color.FromArgb(255, 148, 159, 177)
			}, {
				"pointBackgroundColor", Color.FromArgb(255, 148, 159, 177)
			}, {
				"pointBorderColor", Color.FromArgb(255, 255, 255, 255)
			}, {
				"pointHoverBackgroundColor", Color.FromArgb(255, 255, 255, 204)
			}, {
				"pointHoverBorderColor", Color.FromArgb(255, 148, 159, 177)
			}
		};

		public void SetBackgroundColor(Color color) {
			Colors["backgroundColor"] = color;
		}

		public void SetBorderColor(Color color) {
			Colors["borderColor"] = color;
		}

		public void PointBackgroundColor(Color color) {
			Colors["pointBackgroundColor"] = color;
		}

		public void PointBorderColor(Color color) {
			Colors["pointBorderColor"] = color;
		}

		public void PointHoverBackgroundColor(Color color) {
			Colors["pointHoverBackgroundColor"] = color;
		}

		public void PointHoverBorderColor(Color color) {
			Colors["pointHoverBorderColor"] = color;
		}
	}
}