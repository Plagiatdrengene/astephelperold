using Newtonsoft.Json;

namespace aStepHelper.Data.ChartData {
	public class JsDataSet {
		[JsonProperty("label")] public string Label;
		[JsonProperty("data")] public float[] Data;

		public JsDataSet(string label, float[] data) {
			Label = label;
			Data = data;
		}
	}
}