using Newtonsoft.Json;

namespace aStepHelper.Data.ChartData {
	public class CoordinateSet {
		[JsonProperty("lat")] public double Latitude;
		[JsonProperty("lon")] public double Longitude;

		public CoordinateSet(double latitude, double longitude) {
			Latitude = latitude;
			Longitude = longitude;
		}
	}
}