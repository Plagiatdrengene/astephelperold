using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace aStepHelper.Endpoints {
	[ApiController]
	public abstract class AStepEndPointController : ControllerBase {
		[HttpGet("info")]
		public abstract ContentResult GetInfo();

		[HttpGet("readme")]
		public abstract ContentResult GetReadme();

		[HttpGet("fields")]
		public abstract ContentResult GetFields();

		[HttpPost("render")]
		public abstract ContentResult PostRender(IFormCollection data);
	}
}